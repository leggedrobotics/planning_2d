import numpy as np

def max_rope_product(n):
  m = np.arange(0, n + 1)
  for l in range(0, n + 1):
    max_prod = 1
    for i in range(l):
      prod = m[l - i] * m[i]
      if prod > max_prod:
        max_prod = prod
    m[l] = max_prod
  return m[-1]



if __name__ == "__main__":
  for i in range(31):
    print("Rope length {0} max prod : {1}" .format(i, max_rope_product(i)))
