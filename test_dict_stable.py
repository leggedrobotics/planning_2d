from stable_baselines3 import PPO
from stable_baselines3.common.envs import SimpleMultiObsEnv
from stable_baselines3.common.vec_env import SubprocVecEnv


if __name__ == '__main__':
    env = SimpleMultiObsEnv(random_start=False)
    env = SubprocVecEnv([env for i in range(4)])

    # let's have a look 
    print(env.action_space)
    print(env.observation_space)


    model = PPO("MultiInputPolicy", env, verbose=1)
# model.learn(total_timesteps=1e5)
