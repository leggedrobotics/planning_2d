import gym
from typing import Callable, Dict, List, Optional, Tuple, Type, Union
import torch as th
import torch.nn as nn

from stable_baselines3 import PPO
from stable_baselines3.common.torch_layers import BaseFeaturesExtractor
from stable_baselines3.common.monitor import Monitor


class CustomCNN(BaseFeaturesExtractor):
    """
    :param observation_space: (gym.Space)             
    :param features_dim: (int) Number of features extracted.
        This corresponds to the number of unit for the last layer.
    """

    def __init__(self, observation_space: gym.spaces.Dict, features_dim: int = 256):
        super(CustomCNN, self).__init__(observation_space, features_dim)
        # We assume CxHxW images (channels first)
        # Re-ordering will be done by pre-preprocessing or wrapper
        n_input_channels = observation_space['image'].shape[0]
        self.cnn = nn.Sequential(
            nn.Conv2d(n_input_channels, 32, kernel_size=8, stride=4, padding=0),
            nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=4, stride=2, padding=0),
            nn.ReLU(),
            nn.Flatten(),
        )

        # Compute shape by doing one forward pass
        with th.no_grad():
            n_flatten = self.cnn(
                th.as_tensor(observation_space.sample()[None]).float()
            ).shape[1]

        self.linear = nn.Sequential(nn.Linear(n_flatten, features_dim), nn.ReLU())

    def forward(self, observations: Dict) -> th.Tensor:
        observation = observations['image']
        return self.linear(self.cnn(observations))

policy_kwargs = dict(
    features_extractor_class=CustomCNN,
    features_extractor_kwargs=dict(features_dim=128),
)

import heightgrid.envs
import os 


def make_env(env_name: str, seed:int, log_dir:str) -> Callable[[], gym.Env]:
    """Create custom minigrid environment

    Args:
        env_name (str): name of the minigrid

    Returns:
        init: function that when called instantiate a gym environment
    """
    def init():
        env = gym.make(env_name)
        # default is partially observable
        env.seed(seed)
        env = Monitor(env, log_dir)
        # env = DummyVecEnv([lambda: env])
        # env = VecNormalize(env)

        return env
    return init


log_dir = "tmp/"
os.makedirs(log_dir, exist_ok=True)
env = make_env("HeightGrid-Hole-5x5-v0", 0, log_dir)()


model = PPO("MlpPolicy", env, policy_kwargs=policy_kwargs, verbose=1, tensorboard_log='./log/test/')
print(model)
model.learn(3000, tb_log_name="0")