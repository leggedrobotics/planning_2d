Ѹ
��

B
AssignVariableOp
resource
value"dtype"
dtypetype�
8
Const
output"dtype"
valuetensor"
dtypetype
.
Identity

input"T
output"T"	
Ttype
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
@
StaticRegexFullMatch	
input

output
"
patternstring
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.6.0-dev202105192v1.12.1-56995-g3fd3ae1fbb18��
�
dict_network/mlp/linear_0/bVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*,
shared_namedict_network/mlp/linear_0/b
�
/dict_network/mlp/linear_0/b/Read/ReadVariableOpReadVariableOpdict_network/mlp/linear_0/b*
_output_shapes	
:�*
dtype0
�
dict_network/mlp/linear_0/wVarHandleOp*
_output_shapes
: *
dtype0*
shape:	H�*,
shared_namedict_network/mlp/linear_0/w
�
/dict_network/mlp/linear_0/w/Read/ReadVariableOpReadVariableOpdict_network/mlp/linear_0/w*
_output_shapes
:	H�*
dtype0
�
dict_network/mlp/linear_1/bVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*,
shared_namedict_network/mlp/linear_1/b
�
/dict_network/mlp/linear_1/b/Read/ReadVariableOpReadVariableOpdict_network/mlp/linear_1/b*
_output_shapes	
:�*
dtype0
�
dict_network/mlp/linear_1/wVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*,
shared_namedict_network/mlp/linear_1/w
�
/dict_network/mlp/linear_1/w/Read/ReadVariableOpReadVariableOpdict_network/mlp/linear_1/w* 
_output_shapes
:
��*
dtype0
�
dict_network/mlp/linear_2/bVarHandleOp*
_output_shapes
: *
dtype0*
shape:*,
shared_namedict_network/mlp/linear_2/b
�
/dict_network/mlp/linear_2/b/Read/ReadVariableOpReadVariableOpdict_network/mlp/linear_2/b*
_output_shapes
:*
dtype0
�
dict_network/mlp/linear_2/wVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�*,
shared_namedict_network/mlp/linear_2/w
�
/dict_network/mlp/linear_2/w/Read/ReadVariableOpReadVariableOpdict_network/mlp/linear_2/w*
_output_shapes
:	�*
dtype0
�
"dict_network/atari_torso/conv2_d/bVarHandleOp*
_output_shapes
: *
dtype0*
shape: *3
shared_name$"dict_network/atari_torso/conv2_d/b
�
6dict_network/atari_torso/conv2_d/b/Read/ReadVariableOpReadVariableOp"dict_network/atari_torso/conv2_d/b*
_output_shapes
: *
dtype0
�
"dict_network/atari_torso/conv2_d/wVarHandleOp*
_output_shapes
: *
dtype0*
shape: *3
shared_name$"dict_network/atari_torso/conv2_d/w
�
6dict_network/atari_torso/conv2_d/w/Read/ReadVariableOpReadVariableOp"dict_network/atari_torso/conv2_d/w*&
_output_shapes
: *
dtype0
�
$dict_network/atari_torso/conv2_d/b_1VarHandleOp*
_output_shapes
: *
dtype0*
shape:@*5
shared_name&$dict_network/atari_torso/conv2_d/b_1
�
8dict_network/atari_torso/conv2_d/b_1/Read/ReadVariableOpReadVariableOp$dict_network/atari_torso/conv2_d/b_1*
_output_shapes
:@*
dtype0
�
$dict_network/atari_torso/conv2_d/w_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: @*5
shared_name&$dict_network/atari_torso/conv2_d/w_1
�
8dict_network/atari_torso/conv2_d/w_1/Read/ReadVariableOpReadVariableOp$dict_network/atari_torso/conv2_d/w_1*&
_output_shapes
: @*
dtype0
�
$dict_network/atari_torso/conv2_d/b_2VarHandleOp*
_output_shapes
: *
dtype0*
shape:@*5
shared_name&$dict_network/atari_torso/conv2_d/b_2
�
8dict_network/atari_torso/conv2_d/b_2/Read/ReadVariableOpReadVariableOp$dict_network/atari_torso/conv2_d/b_2*
_output_shapes
:@*
dtype0
�
$dict_network/atari_torso/conv2_d/w_2VarHandleOp*
_output_shapes
: *
dtype0*
shape:@@*5
shared_name&$dict_network/atari_torso/conv2_d/w_2
�
8dict_network/atari_torso/conv2_d/w_2/Read/ReadVariableOpReadVariableOp$dict_network/atari_torso/conv2_d/w_2*&
_output_shapes
:@@*
dtype0
�
dict_network/linear/bVarHandleOp*
_output_shapes
: *
dtype0*
shape:*&
shared_namedict_network/linear/b
{
)dict_network/linear/b/Read/ReadVariableOpReadVariableOpdict_network/linear/b*
_output_shapes
:*
dtype0
�
dict_network/linear/wVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*&
shared_namedict_network/linear/w

)dict_network/linear/w/Read/ReadVariableOpReadVariableOpdict_network/linear/w*
_output_shapes

:*
dtype0

NoOpNoOp
�
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�
value�B� B�
:

_variables
_trainable_variables

signatures
f
0
1
2
3
4
	5

6
7
8
9
10
11
12
13
f
0
1
2
3
4
	5

6
7
8
9
10
11
12
13
 
XV
VARIABLE_VALUEdict_network/mlp/linear_0/b'_variables/0/.ATTRIBUTES/VARIABLE_VALUE
XV
VARIABLE_VALUEdict_network/mlp/linear_0/w'_variables/1/.ATTRIBUTES/VARIABLE_VALUE
XV
VARIABLE_VALUEdict_network/mlp/linear_1/b'_variables/2/.ATTRIBUTES/VARIABLE_VALUE
XV
VARIABLE_VALUEdict_network/mlp/linear_1/w'_variables/3/.ATTRIBUTES/VARIABLE_VALUE
XV
VARIABLE_VALUEdict_network/mlp/linear_2/b'_variables/4/.ATTRIBUTES/VARIABLE_VALUE
XV
VARIABLE_VALUEdict_network/mlp/linear_2/w'_variables/5/.ATTRIBUTES/VARIABLE_VALUE
_]
VARIABLE_VALUE"dict_network/atari_torso/conv2_d/b'_variables/6/.ATTRIBUTES/VARIABLE_VALUE
_]
VARIABLE_VALUE"dict_network/atari_torso/conv2_d/w'_variables/7/.ATTRIBUTES/VARIABLE_VALUE
a_
VARIABLE_VALUE$dict_network/atari_torso/conv2_d/b_1'_variables/8/.ATTRIBUTES/VARIABLE_VALUE
a_
VARIABLE_VALUE$dict_network/atari_torso/conv2_d/w_1'_variables/9/.ATTRIBUTES/VARIABLE_VALUE
b`
VARIABLE_VALUE$dict_network/atari_torso/conv2_d/b_2(_variables/10/.ATTRIBUTES/VARIABLE_VALUE
b`
VARIABLE_VALUE$dict_network/atari_torso/conv2_d/w_2(_variables/11/.ATTRIBUTES/VARIABLE_VALUE
SQ
VARIABLE_VALUEdict_network/linear/b(_variables/12/.ATTRIBUTES/VARIABLE_VALUE
SQ
VARIABLE_VALUEdict_network/linear/w(_variables/13/.ATTRIBUTES/VARIABLE_VALUE
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCallStatefulPartitionedCallsaver_filename/dict_network/mlp/linear_0/b/Read/ReadVariableOp/dict_network/mlp/linear_0/w/Read/ReadVariableOp/dict_network/mlp/linear_1/b/Read/ReadVariableOp/dict_network/mlp/linear_1/w/Read/ReadVariableOp/dict_network/mlp/linear_2/b/Read/ReadVariableOp/dict_network/mlp/linear_2/w/Read/ReadVariableOp6dict_network/atari_torso/conv2_d/b/Read/ReadVariableOp6dict_network/atari_torso/conv2_d/w/Read/ReadVariableOp8dict_network/atari_torso/conv2_d/b_1/Read/ReadVariableOp8dict_network/atari_torso/conv2_d/w_1/Read/ReadVariableOp8dict_network/atari_torso/conv2_d/b_2/Read/ReadVariableOp8dict_network/atari_torso/conv2_d/w_2/Read/ReadVariableOp)dict_network/linear/b/Read/ReadVariableOp)dict_network/linear/w/Read/ReadVariableOpConst*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *&
f!R
__inference__traced_save_6494
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filenamedict_network/mlp/linear_0/bdict_network/mlp/linear_0/wdict_network/mlp/linear_1/bdict_network/mlp/linear_1/wdict_network/mlp/linear_2/bdict_network/mlp/linear_2/w"dict_network/atari_torso/conv2_d/b"dict_network/atari_torso/conv2_d/w$dict_network/atari_torso/conv2_d/b_1$dict_network/atari_torso/conv2_d/w_1$dict_network/atari_torso/conv2_d/b_2$dict_network/atari_torso/conv2_d/w_2dict_network/linear/bdict_network/linear/w*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *)
f$R"
 __inference__traced_restore_6546��
�s
�
__inference_wrapped_module_6337

args_0
args_0_1D
2dict_network_linear_matmul_readvariableop_resource:=
/dict_network_linear_add_readvariableop_resource:^
Ddict_network_atari_torso_conv2_d_convolution_readvariableop_resource: N
@dict_network_atari_torso_conv2_d_biasadd_readvariableop_resource: `
Fdict_network_atari_torso_conv2_d_convolution_1_readvariableop_resource: @P
Bdict_network_atari_torso_conv2_d_biasadd_1_readvariableop_resource:@`
Fdict_network_atari_torso_conv2_d_convolution_2_readvariableop_resource:@@P
Bdict_network_atari_torso_conv2_d_biasadd_2_readvariableop_resource:@K
8dict_network_mlp_linear_0_matmul_readvariableop_resource:	H�D
5dict_network_mlp_linear_0_add_readvariableop_resource:	�L
8dict_network_mlp_linear_1_matmul_readvariableop_resource:
��D
5dict_network_mlp_linear_1_add_readvariableop_resource:	�K
8dict_network_mlp_linear_2_matmul_readvariableop_resource:	�C
5dict_network_mlp_linear_2_add_readvariableop_resource:
identity��7dict_network/atari_torso/conv2_d/BiasAdd/ReadVariableOp�9dict_network/atari_torso/conv2_d/BiasAdd_1/ReadVariableOp�9dict_network/atari_torso/conv2_d/BiasAdd_2/ReadVariableOp�;dict_network/atari_torso/conv2_d/convolution/ReadVariableOp�=dict_network/atari_torso/conv2_d/convolution_1/ReadVariableOp�=dict_network/atari_torso/conv2_d/convolution_2/ReadVariableOp�&dict_network/linear/Add/ReadVariableOp�)dict_network/linear/MatMul/ReadVariableOp�,dict_network/mlp/linear_0/Add/ReadVariableOp�/dict_network/mlp/linear_0/MatMul/ReadVariableOp�,dict_network/mlp/linear_1/Add/ReadVariableOp�/dict_network/mlp/linear_1/MatMul/ReadVariableOp�,dict_network/mlp/linear_2/Add/ReadVariableOp�/dict_network/mlp/linear_2/MatMul/ReadVariableOp�
)dict_network/linear/MatMul/ReadVariableOpReadVariableOp2dict_network_linear_matmul_readvariableop_resource*
_output_shapes

:*
dtype02+
)dict_network/linear/MatMul/ReadVariableOp�
dict_network/linear/MatMulMatMulargs_0_11dict_network/linear/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
dict_network/linear/MatMul�
&dict_network/linear/Add/ReadVariableOpReadVariableOp/dict_network_linear_add_readvariableop_resource*
_output_shapes
:*
dtype02(
&dict_network/linear/Add/ReadVariableOp�
dict_network/linear/AddAddV2$dict_network/linear/MatMul:product:0.dict_network/linear/Add/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
dict_network/linear/Add�
;dict_network/atari_torso/conv2_d/convolution/ReadVariableOpReadVariableOpDdict_network_atari_torso_conv2_d_convolution_readvariableop_resource*&
_output_shapes
: *
dtype02=
;dict_network/atari_torso/conv2_d/convolution/ReadVariableOp�
,dict_network/atari_torso/conv2_d/convolutionConv2Dargs_0Cdict_network/atari_torso/conv2_d/convolution/ReadVariableOp:value:0*
T0*/
_output_shapes
:��������� *
paddingSAME*
strides
2.
,dict_network/atari_torso/conv2_d/convolution�
7dict_network/atari_torso/conv2_d/BiasAdd/ReadVariableOpReadVariableOp@dict_network_atari_torso_conv2_d_biasadd_readvariableop_resource*
_output_shapes
: *
dtype029
7dict_network/atari_torso/conv2_d/BiasAdd/ReadVariableOp�
(dict_network/atari_torso/conv2_d/BiasAddBiasAdd5dict_network/atari_torso/conv2_d/convolution:output:0?dict_network/atari_torso/conv2_d/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:��������� 2*
(dict_network/atari_torso/conv2_d/BiasAdd�
(dict_network/atari_torso/sequential/ReluRelu1dict_network/atari_torso/conv2_d/BiasAdd:output:0*
T0*/
_output_shapes
:��������� 2*
(dict_network/atari_torso/sequential/Relu�
=dict_network/atari_torso/conv2_d/convolution_1/ReadVariableOpReadVariableOpFdict_network_atari_torso_conv2_d_convolution_1_readvariableop_resource*&
_output_shapes
: @*
dtype02?
=dict_network/atari_torso/conv2_d/convolution_1/ReadVariableOp�
.dict_network/atari_torso/conv2_d/convolution_1Conv2D6dict_network/atari_torso/sequential/Relu:activations:0Edict_network/atari_torso/conv2_d/convolution_1/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
20
.dict_network/atari_torso/conv2_d/convolution_1�
9dict_network/atari_torso/conv2_d/BiasAdd_1/ReadVariableOpReadVariableOpBdict_network_atari_torso_conv2_d_biasadd_1_readvariableop_resource*
_output_shapes
:@*
dtype02;
9dict_network/atari_torso/conv2_d/BiasAdd_1/ReadVariableOp�
*dict_network/atari_torso/conv2_d/BiasAdd_1BiasAdd7dict_network/atari_torso/conv2_d/convolution_1:output:0Adict_network/atari_torso/conv2_d/BiasAdd_1/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@2,
*dict_network/atari_torso/conv2_d/BiasAdd_1�
*dict_network/atari_torso/sequential/Relu_1Relu3dict_network/atari_torso/conv2_d/BiasAdd_1:output:0*
T0*/
_output_shapes
:���������@2,
*dict_network/atari_torso/sequential/Relu_1�
=dict_network/atari_torso/conv2_d/convolution_2/ReadVariableOpReadVariableOpFdict_network_atari_torso_conv2_d_convolution_2_readvariableop_resource*&
_output_shapes
:@@*
dtype02?
=dict_network/atari_torso/conv2_d/convolution_2/ReadVariableOp�
.dict_network/atari_torso/conv2_d/convolution_2Conv2D8dict_network/atari_torso/sequential/Relu_1:activations:0Edict_network/atari_torso/conv2_d/convolution_2/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
20
.dict_network/atari_torso/conv2_d/convolution_2�
9dict_network/atari_torso/conv2_d/BiasAdd_2/ReadVariableOpReadVariableOpBdict_network_atari_torso_conv2_d_biasadd_2_readvariableop_resource*
_output_shapes
:@*
dtype02;
9dict_network/atari_torso/conv2_d/BiasAdd_2/ReadVariableOp�
*dict_network/atari_torso/conv2_d/BiasAdd_2BiasAdd7dict_network/atari_torso/conv2_d/convolution_2:output:0Adict_network/atari_torso/conv2_d/BiasAdd_2/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@2,
*dict_network/atari_torso/conv2_d/BiasAdd_2�
*dict_network/atari_torso/sequential/Relu_2Relu3dict_network/atari_torso/conv2_d/BiasAdd_2:output:0*
T0*/
_output_shapes
:���������@2,
*dict_network/atari_torso/sequential/Relu_2�
&dict_network/atari_torso/flatten/ShapeShape8dict_network/atari_torso/sequential/Relu_2:activations:0*
T0*
_output_shapes
:2(
&dict_network/atari_torso/flatten/Shape�
4dict_network/atari_torso/flatten/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 26
4dict_network/atari_torso/flatten/strided_slice/stack�
6dict_network/atari_torso/flatten/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:28
6dict_network/atari_torso/flatten/strided_slice/stack_1�
6dict_network/atari_torso/flatten/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:28
6dict_network/atari_torso/flatten/strided_slice/stack_2�
.dict_network/atari_torso/flatten/strided_sliceStridedSlice/dict_network/atari_torso/flatten/Shape:output:0=dict_network/atari_torso/flatten/strided_slice/stack:output:0?dict_network/atari_torso/flatten/strided_slice/stack_1:output:0?dict_network/atari_torso/flatten/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:*

begin_mask20
.dict_network/atari_torso/flatten/strided_slice�
0dict_network/atari_torso/flatten/concat/values_1Const*
_output_shapes
:*
dtype0*
valueB:@22
0dict_network/atari_torso/flatten/concat/values_1�
,dict_network/atari_torso/flatten/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2.
,dict_network/atari_torso/flatten/concat/axis�
'dict_network/atari_torso/flatten/concatConcatV27dict_network/atari_torso/flatten/strided_slice:output:09dict_network/atari_torso/flatten/concat/values_1:output:05dict_network/atari_torso/flatten/concat/axis:output:0*
N*
T0*
_output_shapes
:2)
'dict_network/atari_torso/flatten/concat�
(dict_network/atari_torso/flatten/ReshapeReshape8dict_network/atari_torso/sequential/Relu_2:activations:00dict_network/atari_torso/flatten/concat:output:0*
T0*'
_output_shapes
:���������@2*
(dict_network/atari_torso/flatten/Reshapev
dict_network/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
dict_network/concat/axis�
dict_network/concatConcatV21dict_network/atari_torso/flatten/Reshape:output:0dict_network/linear/Add:z:0!dict_network/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������H2
dict_network/concat�
/dict_network/mlp/linear_0/MatMul/ReadVariableOpReadVariableOp8dict_network_mlp_linear_0_matmul_readvariableop_resource*
_output_shapes
:	H�*
dtype021
/dict_network/mlp/linear_0/MatMul/ReadVariableOp�
 dict_network/mlp/linear_0/MatMulMatMuldict_network/concat:output:07dict_network/mlp/linear_0/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 dict_network/mlp/linear_0/MatMul�
,dict_network/mlp/linear_0/Add/ReadVariableOpReadVariableOp5dict_network_mlp_linear_0_add_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,dict_network/mlp/linear_0/Add/ReadVariableOp�
dict_network/mlp/linear_0/AddAddV2*dict_network/mlp/linear_0/MatMul:product:04dict_network/mlp/linear_0/Add/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dict_network/mlp/linear_0/Add�
dict_network/mlp/ReluRelu!dict_network/mlp/linear_0/Add:z:0*
T0*(
_output_shapes
:����������2
dict_network/mlp/Relu�
/dict_network/mlp/linear_1/MatMul/ReadVariableOpReadVariableOp8dict_network_mlp_linear_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype021
/dict_network/mlp/linear_1/MatMul/ReadVariableOp�
 dict_network/mlp/linear_1/MatMulMatMul#dict_network/mlp/Relu:activations:07dict_network/mlp/linear_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 dict_network/mlp/linear_1/MatMul�
,dict_network/mlp/linear_1/Add/ReadVariableOpReadVariableOp5dict_network_mlp_linear_1_add_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,dict_network/mlp/linear_1/Add/ReadVariableOp�
dict_network/mlp/linear_1/AddAddV2*dict_network/mlp/linear_1/MatMul:product:04dict_network/mlp/linear_1/Add/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dict_network/mlp/linear_1/Add�
dict_network/mlp/Relu_1Relu!dict_network/mlp/linear_1/Add:z:0*
T0*(
_output_shapes
:����������2
dict_network/mlp/Relu_1�
/dict_network/mlp/linear_2/MatMul/ReadVariableOpReadVariableOp8dict_network_mlp_linear_2_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype021
/dict_network/mlp/linear_2/MatMul/ReadVariableOp�
 dict_network/mlp/linear_2/MatMulMatMul%dict_network/mlp/Relu_1:activations:07dict_network/mlp/linear_2/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2"
 dict_network/mlp/linear_2/MatMul�
,dict_network/mlp/linear_2/Add/ReadVariableOpReadVariableOp5dict_network_mlp_linear_2_add_readvariableop_resource*
_output_shapes
:*
dtype02.
,dict_network/mlp/linear_2/Add/ReadVariableOp�
dict_network/mlp/linear_2/AddAddV2*dict_network/mlp/linear_2/MatMul:product:04dict_network/mlp/linear_2/Add/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
dict_network/mlp/linear_2/Add|
IdentityIdentity!dict_network/mlp/linear_2/Add:z:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp8^dict_network/atari_torso/conv2_d/BiasAdd/ReadVariableOp:^dict_network/atari_torso/conv2_d/BiasAdd_1/ReadVariableOp:^dict_network/atari_torso/conv2_d/BiasAdd_2/ReadVariableOp<^dict_network/atari_torso/conv2_d/convolution/ReadVariableOp>^dict_network/atari_torso/conv2_d/convolution_1/ReadVariableOp>^dict_network/atari_torso/conv2_d/convolution_2/ReadVariableOp'^dict_network/linear/Add/ReadVariableOp*^dict_network/linear/MatMul/ReadVariableOp-^dict_network/mlp/linear_0/Add/ReadVariableOp0^dict_network/mlp/linear_0/MatMul/ReadVariableOp-^dict_network/mlp/linear_1/Add/ReadVariableOp0^dict_network/mlp/linear_1/MatMul/ReadVariableOp-^dict_network/mlp/linear_2/Add/ReadVariableOp0^dict_network/mlp/linear_2/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*]
_input_shapesL
J:���������:���������: : : : : : : : : : : : : : 2r
7dict_network/atari_torso/conv2_d/BiasAdd/ReadVariableOp7dict_network/atari_torso/conv2_d/BiasAdd/ReadVariableOp2v
9dict_network/atari_torso/conv2_d/BiasAdd_1/ReadVariableOp9dict_network/atari_torso/conv2_d/BiasAdd_1/ReadVariableOp2v
9dict_network/atari_torso/conv2_d/BiasAdd_2/ReadVariableOp9dict_network/atari_torso/conv2_d/BiasAdd_2/ReadVariableOp2z
;dict_network/atari_torso/conv2_d/convolution/ReadVariableOp;dict_network/atari_torso/conv2_d/convolution/ReadVariableOp2~
=dict_network/atari_torso/conv2_d/convolution_1/ReadVariableOp=dict_network/atari_torso/conv2_d/convolution_1/ReadVariableOp2~
=dict_network/atari_torso/conv2_d/convolution_2/ReadVariableOp=dict_network/atari_torso/conv2_d/convolution_2/ReadVariableOp2P
&dict_network/linear/Add/ReadVariableOp&dict_network/linear/Add/ReadVariableOp2V
)dict_network/linear/MatMul/ReadVariableOp)dict_network/linear/MatMul/ReadVariableOp2\
,dict_network/mlp/linear_0/Add/ReadVariableOp,dict_network/mlp/linear_0/Add/ReadVariableOp2b
/dict_network/mlp/linear_0/MatMul/ReadVariableOp/dict_network/mlp/linear_0/MatMul/ReadVariableOp2\
,dict_network/mlp/linear_1/Add/ReadVariableOp,dict_network/mlp/linear_1/Add/ReadVariableOp2b
/dict_network/mlp/linear_1/MatMul/ReadVariableOp/dict_network/mlp/linear_1/MatMul/ReadVariableOp2\
,dict_network/mlp/linear_2/Add/ReadVariableOp,dict_network/mlp/linear_2/Add/ReadVariableOp2b
/dict_network/mlp/linear_2/MatMul/ReadVariableOp/dict_network/mlp/linear_2/MatMul/ReadVariableOp:W S
/
_output_shapes
:���������
 
_user_specified_nameargs_0:OK
'
_output_shapes
:���������
 
_user_specified_nameargs_0
�A
�

 __inference__traced_restore_6546
file_prefix;
,assignvariableop_dict_network_mlp_linear_0_b:	�A
.assignvariableop_1_dict_network_mlp_linear_0_w:	H�=
.assignvariableop_2_dict_network_mlp_linear_1_b:	�B
.assignvariableop_3_dict_network_mlp_linear_1_w:
��<
.assignvariableop_4_dict_network_mlp_linear_2_b:A
.assignvariableop_5_dict_network_mlp_linear_2_w:	�C
5assignvariableop_6_dict_network_atari_torso_conv2_d_b: O
5assignvariableop_7_dict_network_atari_torso_conv2_d_w: E
7assignvariableop_8_dict_network_atari_torso_conv2_d_b_1:@Q
7assignvariableop_9_dict_network_atari_torso_conv2_d_w_1: @F
8assignvariableop_10_dict_network_atari_torso_conv2_d_b_2:@R
8assignvariableop_11_dict_network_atari_torso_conv2_d_w_2:@@7
)assignvariableop_12_dict_network_linear_b:;
)assignvariableop_13_dict_network_linear_w:
identity_15��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_2�AssignVariableOp_3�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B'_variables/0/.ATTRIBUTES/VARIABLE_VALUEB'_variables/1/.ATTRIBUTES/VARIABLE_VALUEB'_variables/2/.ATTRIBUTES/VARIABLE_VALUEB'_variables/3/.ATTRIBUTES/VARIABLE_VALUEB'_variables/4/.ATTRIBUTES/VARIABLE_VALUEB'_variables/5/.ATTRIBUTES/VARIABLE_VALUEB'_variables/6/.ATTRIBUTES/VARIABLE_VALUEB'_variables/7/.ATTRIBUTES/VARIABLE_VALUEB'_variables/8/.ATTRIBUTES/VARIABLE_VALUEB'_variables/9/.ATTRIBUTES/VARIABLE_VALUEB(_variables/10/.ATTRIBUTES/VARIABLE_VALUEB(_variables/11/.ATTRIBUTES/VARIABLE_VALUEB(_variables/12/.ATTRIBUTES/VARIABLE_VALUEB(_variables/13/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*P
_output_shapes>
<:::::::::::::::*
dtypes
22
	RestoreV2g
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOp,assignvariableop_dict_network_mlp_linear_0_bIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOpk

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOp.assignvariableop_1_dict_network_mlp_linear_0_wIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_1k

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp.assignvariableop_2_dict_network_mlp_linear_1_bIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_2k

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOp.assignvariableop_3_dict_network_mlp_linear_1_wIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_3k

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp.assignvariableop_4_dict_network_mlp_linear_2_bIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_4k

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOp.assignvariableop_5_dict_network_mlp_linear_2_wIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_5k

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp5assignvariableop_6_dict_network_atari_torso_conv2_d_bIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_6k

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOp5assignvariableop_7_dict_network_atari_torso_conv2_d_wIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_7k

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOp7assignvariableop_8_dict_network_atari_torso_conv2_d_b_1Identity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_8k

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOp7assignvariableop_9_dict_network_atari_torso_conv2_d_w_1Identity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_9n
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOp8assignvariableop_10_dict_network_atari_torso_conv2_d_b_2Identity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_10n
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOp8assignvariableop_11_dict_network_atari_torso_conv2_d_w_2Identity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_11n
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOp)assignvariableop_12_dict_network_linear_bIdentity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_12n
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOp)assignvariableop_13_dict_network_linear_wIdentity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype02
AssignVariableOp_139
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_14Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_14f
Identity_15IdentityIdentity_14:output:0^NoOp_1*
T0*
_output_shapes
: 2
Identity_15�
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*"
_acd_function_control_output(*
_output_shapes
 2
NoOp_1"#
identity_15Identity_15:output:0*1
_input_shapes 
: : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132(
AssignVariableOp_2AssignVariableOp_22(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�*
�
__inference__traced_save_6494
file_prefix:
6savev2_dict_network_mlp_linear_0_b_read_readvariableop:
6savev2_dict_network_mlp_linear_0_w_read_readvariableop:
6savev2_dict_network_mlp_linear_1_b_read_readvariableop:
6savev2_dict_network_mlp_linear_1_w_read_readvariableop:
6savev2_dict_network_mlp_linear_2_b_read_readvariableop:
6savev2_dict_network_mlp_linear_2_w_read_readvariableopA
=savev2_dict_network_atari_torso_conv2_d_b_read_readvariableopA
=savev2_dict_network_atari_torso_conv2_d_w_read_readvariableopC
?savev2_dict_network_atari_torso_conv2_d_b_1_read_readvariableopC
?savev2_dict_network_atari_torso_conv2_d_w_1_read_readvariableopC
?savev2_dict_network_atari_torso_conv2_d_b_2_read_readvariableopC
?savev2_dict_network_atari_torso_conv2_d_w_2_read_readvariableop4
0savev2_dict_network_linear_b_read_readvariableop4
0savev2_dict_network_linear_w_read_readvariableop
savev2_const

identity_1��MergeV2Checkpoints�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Constl
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B'_variables/0/.ATTRIBUTES/VARIABLE_VALUEB'_variables/1/.ATTRIBUTES/VARIABLE_VALUEB'_variables/2/.ATTRIBUTES/VARIABLE_VALUEB'_variables/3/.ATTRIBUTES/VARIABLE_VALUEB'_variables/4/.ATTRIBUTES/VARIABLE_VALUEB'_variables/5/.ATTRIBUTES/VARIABLE_VALUEB'_variables/6/.ATTRIBUTES/VARIABLE_VALUEB'_variables/7/.ATTRIBUTES/VARIABLE_VALUEB'_variables/8/.ATTRIBUTES/VARIABLE_VALUEB'_variables/9/.ATTRIBUTES/VARIABLE_VALUEB(_variables/10/.ATTRIBUTES/VARIABLE_VALUEB(_variables/11/.ATTRIBUTES/VARIABLE_VALUEB(_variables/12/.ATTRIBUTES/VARIABLE_VALUEB(_variables/13/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:06savev2_dict_network_mlp_linear_0_b_read_readvariableop6savev2_dict_network_mlp_linear_0_w_read_readvariableop6savev2_dict_network_mlp_linear_1_b_read_readvariableop6savev2_dict_network_mlp_linear_1_w_read_readvariableop6savev2_dict_network_mlp_linear_2_b_read_readvariableop6savev2_dict_network_mlp_linear_2_w_read_readvariableop=savev2_dict_network_atari_torso_conv2_d_b_read_readvariableop=savev2_dict_network_atari_torso_conv2_d_w_read_readvariableop?savev2_dict_network_atari_torso_conv2_d_b_1_read_readvariableop?savev2_dict_network_atari_torso_conv2_d_w_1_read_readvariableop?savev2_dict_network_atari_torso_conv2_d_b_2_read_readvariableop?savev2_dict_network_atari_torso_conv2_d_w_2_read_readvariableop0savev2_dict_network_linear_b_read_readvariableop0savev2_dict_network_linear_w_read_readvariableopsavev2_const"/device:CPU:0*
_output_shapes
 *
dtypes
22
SaveV2�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity_

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: 2

Identity_1c
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"!

identity_1Identity_1:output:0*�
_input_shapes�
�: :�:	H�:�:
��::	�: : :@: @:@:@@::: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:!

_output_shapes	
:�:%!

_output_shapes
:	H�:!

_output_shapes	
:�:&"
 
_output_shapes
:
��: 

_output_shapes
::%!

_output_shapes
:	�: 

_output_shapes
: :,(
&
_output_shapes
: : 	

_output_shapes
:@:,
(
&
_output_shapes
: @: 

_output_shapes
:@:,(
&
_output_shapes
:@@: 

_output_shapes
::$ 

_output_shapes

::

_output_shapes
: 
�
�
__inference___call___6368
args_0_grid
args_0_state
unknown:
	unknown_0:#
	unknown_1: 
	unknown_2: #
	unknown_3: @
	unknown_4:@#
	unknown_5:@@
	unknown_6:@
	unknown_7:	H�
	unknown_8:	�
	unknown_9:
��

unknown_10:	�

unknown_11:	�

unknown_12:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallargs_0_gridargs_0_stateunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*0
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *(
f#R!
__inference_wrapped_module_63372
StatefulPartitionedCall{
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������2

Identityh
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*]
_input_shapesL
J:���������:���������: : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:\ X
/
_output_shapes
:���������
%
_user_specified_nameargs_0/grid:UQ
'
_output_shapes
:���������
&
_user_specified_nameargs_0/state
�s
�
__inference_wrapped_module_6431
args_0_grid
args_0_stateD
2dict_network_linear_matmul_readvariableop_resource:=
/dict_network_linear_add_readvariableop_resource:^
Ddict_network_atari_torso_conv2_d_convolution_readvariableop_resource: N
@dict_network_atari_torso_conv2_d_biasadd_readvariableop_resource: `
Fdict_network_atari_torso_conv2_d_convolution_1_readvariableop_resource: @P
Bdict_network_atari_torso_conv2_d_biasadd_1_readvariableop_resource:@`
Fdict_network_atari_torso_conv2_d_convolution_2_readvariableop_resource:@@P
Bdict_network_atari_torso_conv2_d_biasadd_2_readvariableop_resource:@K
8dict_network_mlp_linear_0_matmul_readvariableop_resource:	H�D
5dict_network_mlp_linear_0_add_readvariableop_resource:	�L
8dict_network_mlp_linear_1_matmul_readvariableop_resource:
��D
5dict_network_mlp_linear_1_add_readvariableop_resource:	�K
8dict_network_mlp_linear_2_matmul_readvariableop_resource:	�C
5dict_network_mlp_linear_2_add_readvariableop_resource:
identity��7dict_network/atari_torso/conv2_d/BiasAdd/ReadVariableOp�9dict_network/atari_torso/conv2_d/BiasAdd_1/ReadVariableOp�9dict_network/atari_torso/conv2_d/BiasAdd_2/ReadVariableOp�;dict_network/atari_torso/conv2_d/convolution/ReadVariableOp�=dict_network/atari_torso/conv2_d/convolution_1/ReadVariableOp�=dict_network/atari_torso/conv2_d/convolution_2/ReadVariableOp�&dict_network/linear/Add/ReadVariableOp�)dict_network/linear/MatMul/ReadVariableOp�,dict_network/mlp/linear_0/Add/ReadVariableOp�/dict_network/mlp/linear_0/MatMul/ReadVariableOp�,dict_network/mlp/linear_1/Add/ReadVariableOp�/dict_network/mlp/linear_1/MatMul/ReadVariableOp�,dict_network/mlp/linear_2/Add/ReadVariableOp�/dict_network/mlp/linear_2/MatMul/ReadVariableOp�
)dict_network/linear/MatMul/ReadVariableOpReadVariableOp2dict_network_linear_matmul_readvariableop_resource*
_output_shapes

:*
dtype02+
)dict_network/linear/MatMul/ReadVariableOp�
dict_network/linear/MatMulMatMulargs_0_state1dict_network/linear/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
dict_network/linear/MatMul�
&dict_network/linear/Add/ReadVariableOpReadVariableOp/dict_network_linear_add_readvariableop_resource*
_output_shapes
:*
dtype02(
&dict_network/linear/Add/ReadVariableOp�
dict_network/linear/AddAddV2$dict_network/linear/MatMul:product:0.dict_network/linear/Add/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
dict_network/linear/Add�
;dict_network/atari_torso/conv2_d/convolution/ReadVariableOpReadVariableOpDdict_network_atari_torso_conv2_d_convolution_readvariableop_resource*&
_output_shapes
: *
dtype02=
;dict_network/atari_torso/conv2_d/convolution/ReadVariableOp�
,dict_network/atari_torso/conv2_d/convolutionConv2Dargs_0_gridCdict_network/atari_torso/conv2_d/convolution/ReadVariableOp:value:0*
T0*/
_output_shapes
:��������� *
paddingSAME*
strides
2.
,dict_network/atari_torso/conv2_d/convolution�
7dict_network/atari_torso/conv2_d/BiasAdd/ReadVariableOpReadVariableOp@dict_network_atari_torso_conv2_d_biasadd_readvariableop_resource*
_output_shapes
: *
dtype029
7dict_network/atari_torso/conv2_d/BiasAdd/ReadVariableOp�
(dict_network/atari_torso/conv2_d/BiasAddBiasAdd5dict_network/atari_torso/conv2_d/convolution:output:0?dict_network/atari_torso/conv2_d/BiasAdd/ReadVariableOp:value:0*
T0*/
_output_shapes
:��������� 2*
(dict_network/atari_torso/conv2_d/BiasAdd�
(dict_network/atari_torso/sequential/ReluRelu1dict_network/atari_torso/conv2_d/BiasAdd:output:0*
T0*/
_output_shapes
:��������� 2*
(dict_network/atari_torso/sequential/Relu�
=dict_network/atari_torso/conv2_d/convolution_1/ReadVariableOpReadVariableOpFdict_network_atari_torso_conv2_d_convolution_1_readvariableop_resource*&
_output_shapes
: @*
dtype02?
=dict_network/atari_torso/conv2_d/convolution_1/ReadVariableOp�
.dict_network/atari_torso/conv2_d/convolution_1Conv2D6dict_network/atari_torso/sequential/Relu:activations:0Edict_network/atari_torso/conv2_d/convolution_1/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
20
.dict_network/atari_torso/conv2_d/convolution_1�
9dict_network/atari_torso/conv2_d/BiasAdd_1/ReadVariableOpReadVariableOpBdict_network_atari_torso_conv2_d_biasadd_1_readvariableop_resource*
_output_shapes
:@*
dtype02;
9dict_network/atari_torso/conv2_d/BiasAdd_1/ReadVariableOp�
*dict_network/atari_torso/conv2_d/BiasAdd_1BiasAdd7dict_network/atari_torso/conv2_d/convolution_1:output:0Adict_network/atari_torso/conv2_d/BiasAdd_1/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@2,
*dict_network/atari_torso/conv2_d/BiasAdd_1�
*dict_network/atari_torso/sequential/Relu_1Relu3dict_network/atari_torso/conv2_d/BiasAdd_1:output:0*
T0*/
_output_shapes
:���������@2,
*dict_network/atari_torso/sequential/Relu_1�
=dict_network/atari_torso/conv2_d/convolution_2/ReadVariableOpReadVariableOpFdict_network_atari_torso_conv2_d_convolution_2_readvariableop_resource*&
_output_shapes
:@@*
dtype02?
=dict_network/atari_torso/conv2_d/convolution_2/ReadVariableOp�
.dict_network/atari_torso/conv2_d/convolution_2Conv2D8dict_network/atari_torso/sequential/Relu_1:activations:0Edict_network/atari_torso/conv2_d/convolution_2/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@*
paddingSAME*
strides
20
.dict_network/atari_torso/conv2_d/convolution_2�
9dict_network/atari_torso/conv2_d/BiasAdd_2/ReadVariableOpReadVariableOpBdict_network_atari_torso_conv2_d_biasadd_2_readvariableop_resource*
_output_shapes
:@*
dtype02;
9dict_network/atari_torso/conv2_d/BiasAdd_2/ReadVariableOp�
*dict_network/atari_torso/conv2_d/BiasAdd_2BiasAdd7dict_network/atari_torso/conv2_d/convolution_2:output:0Adict_network/atari_torso/conv2_d/BiasAdd_2/ReadVariableOp:value:0*
T0*/
_output_shapes
:���������@2,
*dict_network/atari_torso/conv2_d/BiasAdd_2�
*dict_network/atari_torso/sequential/Relu_2Relu3dict_network/atari_torso/conv2_d/BiasAdd_2:output:0*
T0*/
_output_shapes
:���������@2,
*dict_network/atari_torso/sequential/Relu_2�
&dict_network/atari_torso/flatten/ShapeShape8dict_network/atari_torso/sequential/Relu_2:activations:0*
T0*
_output_shapes
:2(
&dict_network/atari_torso/flatten/Shape�
4dict_network/atari_torso/flatten/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 26
4dict_network/atari_torso/flatten/strided_slice/stack�
6dict_network/atari_torso/flatten/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:28
6dict_network/atari_torso/flatten/strided_slice/stack_1�
6dict_network/atari_torso/flatten/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:28
6dict_network/atari_torso/flatten/strided_slice/stack_2�
.dict_network/atari_torso/flatten/strided_sliceStridedSlice/dict_network/atari_torso/flatten/Shape:output:0=dict_network/atari_torso/flatten/strided_slice/stack:output:0?dict_network/atari_torso/flatten/strided_slice/stack_1:output:0?dict_network/atari_torso/flatten/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
:*

begin_mask20
.dict_network/atari_torso/flatten/strided_slice�
0dict_network/atari_torso/flatten/concat/values_1Const*
_output_shapes
:*
dtype0*
valueB:@22
0dict_network/atari_torso/flatten/concat/values_1�
,dict_network/atari_torso/flatten/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : 2.
,dict_network/atari_torso/flatten/concat/axis�
'dict_network/atari_torso/flatten/concatConcatV27dict_network/atari_torso/flatten/strided_slice:output:09dict_network/atari_torso/flatten/concat/values_1:output:05dict_network/atari_torso/flatten/concat/axis:output:0*
N*
T0*
_output_shapes
:2)
'dict_network/atari_torso/flatten/concat�
(dict_network/atari_torso/flatten/ReshapeReshape8dict_network/atari_torso/sequential/Relu_2:activations:00dict_network/atari_torso/flatten/concat:output:0*
T0*'
_output_shapes
:���������@2*
(dict_network/atari_torso/flatten/Reshapev
dict_network/concat/axisConst*
_output_shapes
: *
dtype0*
value	B :2
dict_network/concat/axis�
dict_network/concatConcatV21dict_network/atari_torso/flatten/Reshape:output:0dict_network/linear/Add:z:0!dict_network/concat/axis:output:0*
N*
T0*'
_output_shapes
:���������H2
dict_network/concat�
/dict_network/mlp/linear_0/MatMul/ReadVariableOpReadVariableOp8dict_network_mlp_linear_0_matmul_readvariableop_resource*
_output_shapes
:	H�*
dtype021
/dict_network/mlp/linear_0/MatMul/ReadVariableOp�
 dict_network/mlp/linear_0/MatMulMatMuldict_network/concat:output:07dict_network/mlp/linear_0/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 dict_network/mlp/linear_0/MatMul�
,dict_network/mlp/linear_0/Add/ReadVariableOpReadVariableOp5dict_network_mlp_linear_0_add_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,dict_network/mlp/linear_0/Add/ReadVariableOp�
dict_network/mlp/linear_0/AddAddV2*dict_network/mlp/linear_0/MatMul:product:04dict_network/mlp/linear_0/Add/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dict_network/mlp/linear_0/Add�
dict_network/mlp/ReluRelu!dict_network/mlp/linear_0/Add:z:0*
T0*(
_output_shapes
:����������2
dict_network/mlp/Relu�
/dict_network/mlp/linear_1/MatMul/ReadVariableOpReadVariableOp8dict_network_mlp_linear_1_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype021
/dict_network/mlp/linear_1/MatMul/ReadVariableOp�
 dict_network/mlp/linear_1/MatMulMatMul#dict_network/mlp/Relu:activations:07dict_network/mlp/linear_1/MatMul/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2"
 dict_network/mlp/linear_1/MatMul�
,dict_network/mlp/linear_1/Add/ReadVariableOpReadVariableOp5dict_network_mlp_linear_1_add_readvariableop_resource*
_output_shapes	
:�*
dtype02.
,dict_network/mlp/linear_1/Add/ReadVariableOp�
dict_network/mlp/linear_1/AddAddV2*dict_network/mlp/linear_1/MatMul:product:04dict_network/mlp/linear_1/Add/ReadVariableOp:value:0*
T0*(
_output_shapes
:����������2
dict_network/mlp/linear_1/Add�
dict_network/mlp/Relu_1Relu!dict_network/mlp/linear_1/Add:z:0*
T0*(
_output_shapes
:����������2
dict_network/mlp/Relu_1�
/dict_network/mlp/linear_2/MatMul/ReadVariableOpReadVariableOp8dict_network_mlp_linear_2_matmul_readvariableop_resource*
_output_shapes
:	�*
dtype021
/dict_network/mlp/linear_2/MatMul/ReadVariableOp�
 dict_network/mlp/linear_2/MatMulMatMul%dict_network/mlp/Relu_1:activations:07dict_network/mlp/linear_2/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2"
 dict_network/mlp/linear_2/MatMul�
,dict_network/mlp/linear_2/Add/ReadVariableOpReadVariableOp5dict_network_mlp_linear_2_add_readvariableop_resource*
_output_shapes
:*
dtype02.
,dict_network/mlp/linear_2/Add/ReadVariableOp�
dict_network/mlp/linear_2/AddAddV2*dict_network/mlp/linear_2/MatMul:product:04dict_network/mlp/linear_2/Add/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������2
dict_network/mlp/linear_2/Add|
IdentityIdentity!dict_network/mlp/linear_2/Add:z:0^NoOp*
T0*'
_output_shapes
:���������2

Identity�
NoOpNoOp8^dict_network/atari_torso/conv2_d/BiasAdd/ReadVariableOp:^dict_network/atari_torso/conv2_d/BiasAdd_1/ReadVariableOp:^dict_network/atari_torso/conv2_d/BiasAdd_2/ReadVariableOp<^dict_network/atari_torso/conv2_d/convolution/ReadVariableOp>^dict_network/atari_torso/conv2_d/convolution_1/ReadVariableOp>^dict_network/atari_torso/conv2_d/convolution_2/ReadVariableOp'^dict_network/linear/Add/ReadVariableOp*^dict_network/linear/MatMul/ReadVariableOp-^dict_network/mlp/linear_0/Add/ReadVariableOp0^dict_network/mlp/linear_0/MatMul/ReadVariableOp-^dict_network/mlp/linear_1/Add/ReadVariableOp0^dict_network/mlp/linear_1/MatMul/ReadVariableOp-^dict_network/mlp/linear_2/Add/ReadVariableOp0^dict_network/mlp/linear_2/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 2
NoOp"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*]
_input_shapesL
J:���������:���������: : : : : : : : : : : : : : 2r
7dict_network/atari_torso/conv2_d/BiasAdd/ReadVariableOp7dict_network/atari_torso/conv2_d/BiasAdd/ReadVariableOp2v
9dict_network/atari_torso/conv2_d/BiasAdd_1/ReadVariableOp9dict_network/atari_torso/conv2_d/BiasAdd_1/ReadVariableOp2v
9dict_network/atari_torso/conv2_d/BiasAdd_2/ReadVariableOp9dict_network/atari_torso/conv2_d/BiasAdd_2/ReadVariableOp2z
;dict_network/atari_torso/conv2_d/convolution/ReadVariableOp;dict_network/atari_torso/conv2_d/convolution/ReadVariableOp2~
=dict_network/atari_torso/conv2_d/convolution_1/ReadVariableOp=dict_network/atari_torso/conv2_d/convolution_1/ReadVariableOp2~
=dict_network/atari_torso/conv2_d/convolution_2/ReadVariableOp=dict_network/atari_torso/conv2_d/convolution_2/ReadVariableOp2P
&dict_network/linear/Add/ReadVariableOp&dict_network/linear/Add/ReadVariableOp2V
)dict_network/linear/MatMul/ReadVariableOp)dict_network/linear/MatMul/ReadVariableOp2\
,dict_network/mlp/linear_0/Add/ReadVariableOp,dict_network/mlp/linear_0/Add/ReadVariableOp2b
/dict_network/mlp/linear_0/MatMul/ReadVariableOp/dict_network/mlp/linear_0/MatMul/ReadVariableOp2\
,dict_network/mlp/linear_1/Add/ReadVariableOp,dict_network/mlp/linear_1/Add/ReadVariableOp2b
/dict_network/mlp/linear_1/MatMul/ReadVariableOp/dict_network/mlp/linear_1/MatMul/ReadVariableOp2\
,dict_network/mlp/linear_2/Add/ReadVariableOp,dict_network/mlp/linear_2/Add/ReadVariableOp2b
/dict_network/mlp/linear_2/MatMul/ReadVariableOp/dict_network/mlp/linear_2/MatMul/ReadVariableOp:\ X
/
_output_shapes
:���������
%
_user_specified_nameargs_0/grid:UQ
'
_output_shapes
:���������
&
_user_specified_nameargs_0/state"�J
saver_filename:0StatefulPartitionedCall:0StatefulPartitionedCall_18"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp:�
l

_variables
_trainable_variables

signatures
__call__
_module"
acme_snapshot
�
0
1
2
3
4
	5

6
7
8
9
10
11
12
13"
trackable_tuple_wrapper
�
0
1
2
3
4
	5

6
7
8
9
10
11
12
13"
trackable_tuple_wrapper
"
signature_map
*:(�2dict_network/mlp/linear_0/b
.:,	H�2dict_network/mlp/linear_0/w
*:(�2dict_network/mlp/linear_1/b
/:-
��2dict_network/mlp/linear_1/w
):'2dict_network/mlp/linear_2/b
.:,	�2dict_network/mlp/linear_2/w
0:. 2"dict_network/atari_torso/conv2_d/b
<:: 2"dict_network/atari_torso/conv2_d/w
0:.@2"dict_network/atari_torso/conv2_d/b
<:: @2"dict_network/atari_torso/conv2_d/w
0:.@2"dict_network/atari_torso/conv2_d/b
<::@@2"dict_network/atari_torso/conv2_d/w
#:!2dict_network/linear/b
':%2dict_network/linear/w
�2�
__inference___call___6368�
���
FullArgSpec
args�
jself
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
__inference_wrapped_module_6431�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 �
__inference___call___6368�
	z�w
p�m
k�h
5
grid-�*
args_0/grid���������
/
state&�#
args_0/state���������
� "�����������
__inference_wrapped_module_6431�
	z�w
p�m
k�h
5
grid-�*
args_0/grid���������
/
state&�#
args_0/state���������
� "����������