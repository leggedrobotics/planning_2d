import datetime
import os
from time import time 
# os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from absl import app
from absl import flags
from acme.tf import networks
from heightgrid.dm_envs import HoleEnv5x5, HoleEnv32x32, HoleEnv5x5_1x1
from acme.tf.networks import base
import acme
from acme import specs
from acme import wrappers
from acme.agents.tf import dqn
from acme.utils import loggers
import bsuite
import sonnet as snt
import numpy as np
import tensorflow as tf
import os 


class CustomTorso(base.Module):
  """Simple convolutional stack commonly used for Atari."""

  def __init__(self):
    super().__init__(name='atari_torso')
    self._network = snt.Sequential([
        snt.Conv2D(32, [8, 8], [4, 4]),
        tf.nn.relu,
        snt.Conv2D(64, [4, 4], [2, 2]),
        tf.nn.relu,
        snt.Conv2D(64, [3, 3], [1, 1]),
        tf.nn.relu,
        snt.Flatten(),
    ])
 
  def __call__(self, inputs) -> tf.Tensor:
    # print("input type ", inputs.dtype, inputs.shape)
    # return self._network(inputs)
    idx = tf.shape(inputs)[0]
    return tf.random.uniform(shape=(idx, 5))

class CustomTorso2(base.Module):
  """Simple convolutional stack commonly used for Atari."""

  def __init__(self):
    super().__init__(name='atari_torso')
    self._network = snt.Sequential([
        snt.Conv2D(64, [3, 3], [1, 1]),
        tf.nn.relu,
        # snt.Conv2D(64, [4, 4], [2, 2]),
        # tf.nn.relu,
        # snt.Conv2D(64, [3, 3], [1, 1]),
        # tf.nn.relu,
        snt.Flatten(),
    ])
  

  def __call__(self, inputs) -> tf.Tensor:
    # print("input type ", inputs.dtype, inputs.shape)
    return self._network(inputs)



class DictNetwork(snt.Module):

  def __init__(self, output_size:int, name=None):
      super().__init__(name=name)
      self._output_size = output_size
      self._torso = CustomTorso2()
      self._mpl = snt.nets.MLP([128, self._output_size])
      self.linear_state = snt.Linear(10)
      # self._mpl = snt.nets.MLP([self._output_size])
      # reducing the size of the network only marginally improve the execution speed 
  
  # @snt.once
  # def _initialize(self, x):
  #   image_features = self._torso(['image'])
  #   x = tf.concat([image_features, x['vec']], axis=1)

  def __call__(self, x):
    # x_grid = self._torso(x['grid'])
    # print(x_grid.shape)
    x_state = self.linear_state(x['state'])
    # print(x_state)
    x = tf.concat([self._torso(x['grid']), x_state], axis=1)
    x = self._mpl(x)
    return x


def main():
  environment = HoleEnv5x5_1x1()
  environment = wrappers.SinglePrecisionWrapper(environment)
  environment_spec = specs.make_environment_spec(environment)
  # print(environment_spec

  # environment.reset()
  # num_steps = 10000
  # import time
  # time_start = time.time()
  # for i in range(num_steps):
  #   _ = environment.step(np.int32(1))
  # time_end = time.time()
  # print("steps per sec ", num_steps / (time_end - time_start)) it's 16000 btw


  checkpoint_path = os.getcwd() + "/log/"
  new_uid = True
  checkpoint_path = "/home/lorenzo/git/planning_2d/log"
  network = DictNetwork(output_size=5)
  # # Construct the agent.
  agent = dqn.DQN(
      environment_spec=environment_spec, 
      network=network,
      checkpoint_subpath=checkpoint_path,
      add_uid=new_uid,
      time_delta=10.,
      min_replay_size=1000)

  # # Run the environment loop.
  
  logger_csv = loggers.CSVLogger(checkpoint_path, time_delta=10., add_uid=new_uid)
  logger_terminal = loggers.TerminalLogger(print_fn=print, time_delta=10.)
  logger = loggers.Dispatcher([logger_csv, logger_terminal])
  loop = acme.EnvironmentLoop(environment, agent, logger=logger)

  # tf.profiler.experimental.start(checkpoint_path)
  # Train the model here
  loop.run(num_episodes=100000)  # pytype: disable=attribute-error
  # tf.profiler.experimental.stop()


if __name__ == '__main__':
  main()  
  # checkpoint_path = "/home/lorenzo/git/planning_2d/log/fed68b00-b8cb-11eb-812c-bba6f8f69ddd/snapshots/network"
  # model = tf.keras.models.load_model(checkpoint_path)
  # # model.summary()

  # environment = HoleEnv5x5()
  # environment = wrappers.SinglePrecisionWrapper(environment)
  # environment_spec = specs.make_environment_spec(environment)

  # timestep = environment.reset()
  # print(model.)
  # model.select_action(timestep.observation)
