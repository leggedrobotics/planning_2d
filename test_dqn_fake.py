import datetime
import os
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from absl import app
from absl import flags
from acme.tf import networks
from acme.wrappers.single_precision import SinglePrecisionWrapper
from heightgrid.dm_envs import HoleEnv5x5, HoleEnv32x32
from acme.tf.networks import base
import acme
from acme import specs
from acme import wrappers
from acme.agents.tf import dqn
from acme.utils import loggers
from acme.testing import fakes
import bsuite
import sonnet as snt
import numpy as np
import tensorflow as tf
import os 


class CustomTorso(base.Module):
  """Simple convolutional stack commonly used for Atari."""

  def __init__(self):
    super().__init__(name='atari_torso')
    self._network = snt.Sequential([
        snt.Conv2D(32, [8, 8], [4, 4]),
        tf.nn.relu,
        snt.Conv2D(64, [4, 4], [2, 2]),
        tf.nn.relu,
        snt.Conv2D(64, [3, 3], [1, 1]),
        tf.nn.relu,
        snt.Flatten(),
    ])

  def __call__(self, inputs) -> tf.Tensor:
  # print("input type ", inputs.dtype, inputs.shape)
    return self._network(inputs)

  
class CustomTorso2(base.Module):
  """Simple convolutional stack commonly used for Atari."""

  def __init__(self):
    super().__init__(name='atari_torso')
    self._network = snt.Sequential([
        snt.Conv2D(32, [3, 3], [1, 1]),
        # tf.nn.relu,
        # snt.Conv2D(64, [4, 4], [2, 2]),
        # tf.nn.relu,
        # snt.Conv2D(64, [3, 3], [1, 1]),
        tf.nn.relu,
        snt.Flatten(),
    ])
  

  def __call__(self, inputs) -> tf.Tensor:
    # print("input type ", inputs.dtype, inputs.shape)
    return self._network(inputs)


class DictNetwork(snt.Module):

  def __init__(self, output_size:int, name=None):
      super().__init__(name=name)
      self._output_size = output_size
      self._torso = CustomTorso2()
      self._mpl = snt.nets.MLP([16, self._output_size])
      # self._mpl = snt.nets.MLP([128, self._output_size])
      # reducing the size of the network only marginally improve the execution speed 
  
  # @snt.once
  # def _initialize(self, x):
  #   image_features = self._torso(['image'])
  #   x = tf.concat([image_features, x['vec']], axis=1)

  def __call__(self, x):
    x = self._torso(x)
    x = self._mpl(x)
    return x


def main():

  environment = fakes.DiscreteEnvironment(
      num_actions=5,
      num_observations=1,
      obs_shape=(10, 10, 3),
      obs_dtype=np.float32,
      episode_length=1000)  # print(environment_spec
  environment = SinglePrecisionWrapper(environment)

  num_steps = 100000
  import time
  time_start = time.time()
  for i in range(num_steps):
    _ = environment.step(np.int32(0))
  time_end = time.time()
  print("steps per sec ", num_steps / (time_end - time_start))
  environment_spec = specs.make_environment_spec(environment)
  
  checkpoint_path = os.getcwd() + "/log"
  network = DictNetwork(output_size=5)
  # # Construct the agent.
  agent = dqn.DQN(
      environment_spec=environment_spec, network=network)

  # # Run the environment loop.
  
  logger_csv = loggers.CSVLogger(checkpoint_path)
  logger_terminal = loggers.TerminalLogger(print_fn=print, time_delta=10.)
  logger = loggers.Dispatcher([logger_csv, logger_terminal])
  loop = acme.EnvironmentLoop(environment, agent, logger=logger_terminal)

  # tf.profiler.experimental.start(checkpoint_path)
  # Train the model here
  loop.run(num_episodes=10000)  # pytype: disable=attribute-error
  # tf.profiler.experimental.stop()



if __name__ == '__main__':
  main()
