import datetime
import os
from time import time 
# os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import matplotlib
from tensorflow.python import util
from tensorflow.python.ops.gen_array_ops import strided_slice
from tensorflow.python.ops.gen_nn_ops import MaxPool, MaxPoolV2
matplotlib.use('TkAgg')
from absl import app
from absl import flags
from acme.tf import networks
from heightgrid.dm_envs import HoleEnv5x5, HoleEnv32x32, HoleEnv5x5_1x1
from acme.tf.networks import base
import acme
from acme import specs
from acme import wrappers
from acme.agents.tf import dqn
from acme.utils import loggers
import bsuite
from heightgrid.dm_envs.empty import EmptyEnv5x5, EmptyRandomEnv5x5, EmptyEnv32x32, EmptyRandomEnv32x32
import sonnet as snt
import numpy as np
import tensorflow as tf
import os 
from sonnet.src import utils 
from typing import Optional, Sequence, Text, Union


class MaxPooling2D(snt.Module):

  def __init__(self, 
               ksize: Union[int, Sequence[int]],
               strides: Union[int, Sequence[int]], 
               padding: Text = "VALID", 
               dataformat: Text = None,
               name: Optional[Text] = None):
      super().__init__(name=name)
      self._stride = strides
      self._ksize = ksize
      self._padding = padding
      self._layer = None
  
  def __call__(self, inputs):
    return tf.nn.max_pool2d(inputs, self._ksize, self._stride, padding=self._padding)


class CustomTorso(base.Module):
  """Simple convolutional stack commonly used for Atari."""

  def __init__(self):
    super().__init__(name='atari_torso')
    self._network = snt.Sequential([
        snt.Conv2D(32, [8, 8], [4, 4]),
        tf.nn.relu,
        snt.Conv2D(64, [4, 4], [2, 2]),
        tf.nn.relu,
        snt.Conv2D(64, [3, 3], [1, 1]),
        tf.nn.relu,
        snt.Flatten(),
    ])
 
  def __call__(self, inputs) -> tf.Tensor:
    print("input type ", inputs.dtype, inputs.shape)
    return self._network(inputs)

class CustomTorso2(base.Module):
  """Simple convolutional stack commonly used for Atari."""

  def __init__(self):
    super().__init__(name='atari_torso')
    self._network = snt.Sequential([
        snt.Conv2D(64, [3, 3], [1, 1]),
        tf.nn.relu,
        snt.Conv2D(64, [4, 4], [2, 2]),
        tf.nn.relu,
        snt.Conv2D(64, [3, 3], [1, 1]),
        tf.nn.relu,
        snt.Flatten(),
    ])
  

  def __call__(self, inputs) -> tf.Tensor:
    # print("input type ", inputs.dtype, inputs.shape)
    return self._network(inputs)


class CustomTorso3(base.Module):
  """Simple convolutional stack commonly used for Atari."""

  def __init__(self):
    super().__init__(name='atari_torso')
    self._network = snt.Sequential([
        snt.Conv2D(16, [2, 2], [1, 1]),
        tf.nn.relu,
        MaxPooling2D(ksize=(2, 2), strides=1),
        snt.Conv2D(32, [2, 2], [1, 1]),
        tf.nn.relu,
        # snt.Conv2D(64, [3, 3], [1, 1]),
        # tf.nn.relu,
        snt.Flatten(),
    ])
  

  def __call__(self, inputs) -> tf.Tensor:
    # print("input type ", inputs.dtype, inputs.shape)
    return self._network(inputs)


class DictNetwork(snt.Module):

  def __init__(self, output_size:int, name=None):
      super().__init__(name=name)
      self._output_size = output_size
      self._torso = CustomTorso()
      self._mpl = snt.nets.MLP([128, 128, self._output_size])
      self.linear_state = snt.Linear(8)
      # self._mpl = snt.nets.MLP([self._output_size])
      # reducing the size of the network only marginally improve the execution speed 
  
  # @snt.once
  # def _initialize(self, x):
  #   image_features = self._torso(['image'])
  #   x = tf.concat([image_features, x['vec']], axis=1)

  def __call__(self, x):
    # x_grid = self._torso(x['grid'])
    # print(x_grid.shape)
    x_state = self.linear_state(x['state'])
    x_torso = self._torso(x['grid'])
    # print(x_torso.shape)
    x = tf.concat([x_torso, x_state], axis=1)
    x = self._mpl(x)
    return x


def main():
  environment = EmptyRandomEnv5x5()
  environment = wrappers.SinglePrecisionWrapper(environment)
  environment_spec = specs.make_environment_spec(environment)
  # environment.render(block=True)
  # print(environment_spec

  # environment.reset()
  # num_steps = 10000
  # import time
  # time_start = time.time()
  # for i in range(num_steps):
  #   _ = environment.step(np.int32(1))
  # time_end = time.time()
  # print("steps per sec ", num_steps / (time_end - time_start)) it's 16000 btw


  checkpoint_path = os.getcwd() + "/log/"
  new_uid = False
  checkpoint_path = "/home/lorenzo/git/planning_2d/log/2a12d87c-bada-11eb-b33b-5ddaf5e0d718"
  network = DictNetwork(output_size=3)

  # # Construct the agent.
  agent = dqn.DQN(
      environment_spec=environment_spec, 
      network=network,
      checkpoint_subpath=checkpoint_path,
      add_uid=new_uid,
      time_delta=1.,
      min_replay_size=1000,)

  # # Run the environment loop.
  
  logger_csv = loggers.CSVLogger(checkpoint_path, time_delta=1., add_uid=new_uid)
  logger_terminal = loggers.TerminalLogger(print_fn=print, time_delta=10.)
  logger = loggers.Dispatcher([logger_csv, logger_terminal])
  loop = acme.EnvironmentLoop(environment, agent, logger=logger)

  # tf.profiler.experimental.start(checkpoint_path)
  # Train the model here
  loop.run(num_episodes=2000)  # pytype: disable=attribute-error
  # tf.profiler.experimental.stop()



import imageio
import base64

def save_video(frames, filename='temp.mp4'):
    """Save and display video."""
    # Write video
    with imageio.get_writer(filename, fps=2) as video:
        for frame in frames:
            video.append_data(frame)
    # Read video and display the video
    video = open(filename, 'rb').read()
    b64_video = base64.b64encode(video)
    # video_tag = ('<video  width="320" height="240" controls alt="test" '
                # 'src="data:video/mp4;base64,{0}">').format(b64_video.decode())

#   return IPython.display.HTML(video_tag)

def render(env):
  img, img_target =  env.environment.render(mode='rgb_array')
  return img

if __name__ == '__main__':
  main()  

  produce_video = 0
  if produce_video:
    environment = EmptyRandomEnv5x5()
    environment = wrappers.SinglePrecisionWrapper(environment)
    environment_spec = specs.make_environment_spec(environment)

    checkpoint_path = "/home/lorenzo/git/planning_2d/log/2a12d87c-bada-11eb-b33b-5ddaf5e0d718"
    new_uid = False


    network = DictNetwork(output_size=3)

    agent = dqn.DQN(
        environment_spec=environment_spec, 
        network=network,
        checkpoint_subpath=checkpoint_path,
        add_uid=new_uid,
        time_delta=10.,
        min_replay_size=1000)

    agent._learner._
    # Run the actor in the environment for desired number of steps.
    frames = []
    num_steps = 100
    timestep = environment.reset()

    for i in range(num_steps):
      print(i)
      frames.append(render(environment))
      action = agent.select_action(timestep.observation)
      timestep = environment.step(action)
      # print(timestep.last())
      if timestep.last():
        timestep = environment.reset()
    
    save_video(np.array(frames))

# Save video of the behaviour.

  # checkpoint_path = "/home/lorenzo/git/planning_2d/log/fed68b00-b8cb-11eb-812c-bba6f8f69ddd/snapshots/network"
  # model = tf.keras.models.load_model(checkpoint_path)
  # # model.summary()

  # environment = HoleEnv5x5()
  # environment = wrappers.SinglePrecisionWrapper(environment)
  # environment_spec = specs.make_environment_spec(environment)

  # timestep = environment.reset()
  # print(model.)
  # model.select_action(timestep.observation)
