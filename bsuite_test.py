import bsuite

env = bsuite.load_from_id('catch/0')

from bsuite import sweep
import numpy as np

print(sweep.DEEP_SEA)

num_actions = env.action_spec().num_values

print(env.action_spec())

for _ in range(10):
    timestep = env.reset()
    while not timestep.last():
        action = np.random.randint(0, 3)
        timestep = env.step(action)
        print(timestep)

