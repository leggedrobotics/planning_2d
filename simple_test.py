from typing import Callable, Dict, List, Optional, Tuple, Type, Union
from numpy.random import random
from stable_baselines3 import PPO
from stable_baselines3.common.vec_env import SubprocVecEnv
from stable_baselines3.common.monitor import Monitor
import torch.nn as nn
import torch as th
import gym
import numpy as np
import os 
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.evaluation import evaluate_policy
import heightgrid.envs


def make_env(env_name: str, seed:int, log_dir:str) -> Callable[[], gym.Env]:
    """Create custom minigrid environment

    Args:
        env_name (str): name of the minigrid

    Returns:
        init: function that when called instantiate a gym environment
    """
    def init():
        env = gym.make(env_name)
        # default is partially observable
        env.seed(seed)
        env = Monitor(env, log_dir)
        # env = DummyVecEnv([lambda: env])
        # env = VecNormalize(env)

        return env
    return init


def linear_schedule(initial_value: float) -> Callable[[float], float]:

    def func(progress: float) -> float: 
        """
        Args:
            progress (float): decreases from 1 to 0 (at the end)        
            Returns: learning rate 
        """
        return progress * initial_value
    
    return func


def random_policy():
    return np.random.randint(0, 6)


from stable_baselines3.common.policies import ActorCriticPolicy


class CustomNetwork(nn.Module):
    """
    Custom network for policy and value function.
    It receives as input the features extracted by the feature extractor.

    :param feature_dim: dimension of the features extracted with the features_extractor (e.g. features from a CNN)
    :param last_layer_dim_pi: (int) number of units for the last layer of the policy network
    :param last_layer_dim_vf: (int) number of units for the last layer of the value network
    """

    def __init__(
        self,
        feature_dim: int,
        last_layer_dim_pi: int = 64,
        last_layer_dim_vf: int = 64,
    ):
        super(CustomNetwork, self).__init__()

        # IMPORTANT:
        # Save output dimensions, used to create the distributions
        self.latent_dim_pi = last_layer_dim_pi
        self.latent_dim_vf = last_layer_dim_vf

        # Policy network
        self.policy_net = nn.Sequential(
            nn.Linear(feature_dim, last_layer_dim_pi), nn.ReLU()
        )
        # Value network
        self.value_net = nn.Sequential(
            nn.Linear(feature_dim, last_layer_dim_vf), nn.ReLU()
        )

    def forward(self, features: th.Tensor) -> Tuple[th.Tensor, th.Tensor]:
        """
        :return: (th.Tensor, th.Tensor) latent_policy, latent_value of the specified network.
            If all layers are shared, then ``latent_policy == latent_value``
        """
        
        return self.policy_net(features), self.value_net(features)


class CustomActorCriticPolicy(ActorCriticPolicy):
    def __init__(
        self,
        observation_space: gym.spaces.Space,
        action_space: gym.spaces.Space,
        lr_schedule: Callable[[float], float],
        net_arch: Optional[List[Union[int, Dict[str, List[int]]]]] = None,
        activation_fn: Type[nn.Module] = nn.Tanh,
        *args,
        **kwargs,
    ):

        super(CustomActorCriticPolicy, self).__init__(
            observation_space,
            action_space,
            lr_schedule,
            net_arch,
            activation_fn,
            # Pass remaining arguments to base class
            *args,
            **kwargs,
        )
        # Disable orthogonal initialization
        self.ortho_init = False

    def _build_mlp_extractor(self) -> None:
        self.mlp_extractor = CustomNetwork(self.features_dim)



if __name__ == '__main__':
    log_dir = "tmp/"
    os.makedirs(log_dir, exist_ok=True)
    # env = make_env("HeightGrid-Hole-5x5-v0", 0, log_dir)()
    # print(env.observation_space)
    env = SubprocVecEnv([make_env("HeightGrid-Hole-5x5-v0", i, log_dir) for i in range(4)])
    # print(env[0].observation_space)
    # env.reset()
    # for i in range(10):
    #     action = random_policy()
    #     obs, reward, done, info = env.step(action)
    #     print("action ", action)
    #     print("reward {0:.2f} and done {1}" .format(reward, done))

    model = PPO("MultiInputPolicy", env, verbose=1)
    timesteps = 1000000
    # spotimodel.learn(timesteps)
    
    # obs = env.reset()
    # print(obs)
    # # obs = env.reset()
    # # model.learn(total_timesteps=timesteps)
    # for i in range(timesteps):
    #     # action, _states = model.predict(obs.image)
    #     action = 0
    #     obs, reward, done, info = env.step(action)
    #     print(done, action)
    #     env.render()
    #     if done:
    #         obs = env.reset()
