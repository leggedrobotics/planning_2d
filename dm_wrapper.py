import dm_env
import numpy as np
 

class HeightGridEnv(dm_env.Environment):
    """Wrapper of heightgrid for a dm environment
    """
    def __init__(self, env) -> None:
        self._rgn = env.seed